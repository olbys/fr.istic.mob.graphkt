package graphkt.mob.istic.fr.graphkt;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.RectF;
import android.media.Image;
import android.support.v4.view.GestureDetectorCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import static android.content.ContentValues.TAG;

public class EditNodeView extends View implements AdapterView.OnItemSelectedListener{

    Dialog dialog;
    Dialog label;
    Dialog couleur;
    Dialog taille;
    Dialog labelArc;
    TextView closeDialog;
    ImageView img_delete;
    ImageView img_color;
    ImageView img_label;
    ImageView img_length;
    EditText edit_text_label;
    EditText edit_text_length;
    EditText edit_text_arc_label;
    Button save;
    Context context;
    int selected_node;
    int selected_arc;
    String new_label;
    String new_arc_label;

    private DrawableGraph drawableGraph ;


    public EditNodeView(Context context, DrawableGraph drawableGraphInput){

        super(context);
        this.context=context;
        this.selected_node=-1;
        this.drawableGraph = drawableGraphInput;
        this.drawableGraph.setColor(Color.RED);
        dialog = new Dialog(this.getContext());
        couleur = new Dialog(this.getContext());
        label = new Dialog(this.getContext());
        labelArc = new Dialog(this.getContext());
        taille = new Dialog(this.getContext());
        edit_text_label = new EditText(this.getContext());
        save = new Button(this.getContext());

    }


    public EditNodeView(Context context){
        super(context);
        Point size = new Point(1440, 2392);
        this.drawableGraph = new DrawableGraph(size);
        this.drawableGraph.setColor(Color.RED);
    }


    public void setDrawableGraph(DrawableGraph drawableGraph) {
        this.drawableGraph = drawableGraph;
    }

    protected void onDraw(Canvas canvas) {
        this.drawableGraph.draw(canvas);
    }


    private void ShowEditDialog(final int node){

        Log.e("TEST DU LONG CLICK", "ShowEditDialog: la position du node est "+ node);
        this.selected_node=node;
        dialog.setContentView(R.layout.editnodedialog);

        img_delete=(ImageView) dialog.findViewById(R.id.img_delete);
        img_color=(ImageView) dialog.findViewById(R.id.img_color);
        img_label=(ImageView) dialog.findViewById(R.id.img_label);
        img_length=(ImageView) dialog.findViewById(R.id.img_length);

        closeDialog = (TextView) dialog.findViewById(R.id.txtclose);
        closeDialog.setOnClickListener(new View.OnClickListener(){
           @Override
            public void onClick(View v){
               dialog.dismiss();
           }
        });

        img_delete.setOnClickListener(new OnClickListener() {
          @Override
          public void onClick(View v) {
              delete_node(node);
              dialog.dismiss();

          }
        });
        img_color.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ShowEditColorDialog(node);


            }
        });

        img_label.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ShowEditLabelDialog(node);


            }
        });
        img_length.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                ShowEditLengthDialog(node);


            }
        });



        dialog.show();

    }

    private void ShowEditArcDialog(final int arc){
        this.selected_arc=arc;
        labelArc.setContentView(R.layout.arclabeldialog);
        closeDialog = (TextView) labelArc.findViewById(R.id.label_txtclose);
        edit_text_arc_label = (EditText) labelArc.findViewById(R.id.edit_text_arc_label);
        save = (Button) labelArc.findViewById(R.id.btn_save_label);
        closeDialog.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                labelArc.dismiss();
            }
        });
        save.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                new_arc_label = edit_text_arc_label.getText().toString();
                drawableGraph.getGraph().getArcGraph().get(arc).setEtiquette(new_arc_label);
                invalidate();
                labelArc.dismiss();
            }
        });

        labelArc.show();

    }

    private void ShowEditColorDialog(int node){

        couleur.setContentView(R.layout.couleurdialog);
        closeDialog = (TextView) couleur.findViewById(R.id.color_txtclose);
        save = (Button) couleur.findViewById(R.id.btn_save);
        closeDialog.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                couleur.dismiss();
            }
        });
        save.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                couleur.dismiss();
            }
        });
        Spinner spinner = (Spinner) couleur.findViewById(R.id.color_spinner);
        spinner.setOnItemSelectedListener(this);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getContext(),R.array.couleur_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        couleur.show();

    }

    private void ShowEditLabelDialog(int node){

        label.setContentView(R.layout.labeldialog);
        closeDialog = (TextView) label.findViewById(R.id.label_txtclose);
        edit_text_label = (EditText) label.findViewById(R.id.edit_text_label);
        save = (Button) label.findViewById(R.id.btn_save_label);
        closeDialog.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                label.dismiss();
            }
        });
        save.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                 new_label = edit_text_label.getText().toString();
                drawableGraph.getGraph().getNodeLabels().set(selected_node,new_label);
                drawableGraph.nodeEtiquetteAdapter(selected_node , new_label);
                invalidate();
                label.dismiss();
            }
        });

        label.show();

    }

    private void ShowEditLengthDialog(int node){

        taille.setContentView(R.layout.tailledialog);

        closeDialog = (TextView) taille.findViewById(R.id.label_txtclose);

        edit_text_length = (EditText) taille.findViewById(R.id.edit_text_length);

        save = (Button) taille.findViewById(R.id.btn_save);

        closeDialog.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                taille.dismiss();
            }
        });

        save.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                if (TextUtils.isEmpty(edit_text_length.getText().toString())){
                    Toast.makeText(taille.getContext(), R.string.length_error, Toast.LENGTH_SHORT).show();
                }
                else {
                    String text_length = edit_text_length.getText().toString();
                    Float new_length = Float.valueOf(text_length);
                    drawableGraph.nodeLengthAdapter(selected_node, new_length);
                    invalidate();
                    taille.dismiss();
                }

            }
        });

        taille.show();

    }

    private void delete_node(int node){

        // suppression des arcs lies au noeud en question
        RectF node_to_delete = this.drawableGraph.getGraph().getNodes().get(node);
        this.drawableGraph.suppresLink(node_to_delete);
        drawableGraph.getGraph().getNodes().remove(node);
        drawableGraph.getGraph().getNodeColors().remove(node);
        drawableGraph.getGraph().getNodeLabels().remove(node);

        invalidate();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        switch (position){
            case 1:
                drawableGraph.getGraph().getNodeColors().set(selected_node,Color.RED);
                invalidate();
                break;

            case 2:
                drawableGraph.getGraph().getNodeColors().set(selected_node,Color.GREEN);
                invalidate();
                break;

            case 3:
                drawableGraph.getGraph().getNodeColors().set(selected_node,Color.BLUE);
                invalidate();
                break;

            case 4:
                drawableGraph.getGraph().getNodeColors().set(selected_node,Color.rgb(255, 165, 0));
                invalidate();
                break;

            case 5:
                drawableGraph.getGraph().getNodeColors().set(selected_node,Color.CYAN);
                invalidate();
                break;

            case 6:
                drawableGraph.getGraph().getNodeColors().set(selected_node,Color.MAGENTA);
                invalidate();
                break;

            case 7:
                drawableGraph.getGraph().getNodeColors().set(selected_node,Color.BLACK);
                invalidate();
                break;

        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    final GestureDetector gestureDetector = new GestureDetector(new GestureDetector.SimpleOnGestureListener() {

        public void onLongPress(MotionEvent event) {
            Log.e("", "Longpress detected");
            Point touchDown = new Point((int)event.getX(), (int)event.getY());
            int node = drawableGraph.getIntersectionNodesIndex(touchDown);
            int arc = drawableGraph.etiquetteOfArc(touchDown) ;
            if(node != -1)
                    ShowEditDialog(node);
            else if(arc != -1)
                    ShowEditArcDialog(arc);

        }
        public boolean onDown(MotionEvent arg0) {
            return true;
        }

        public boolean onFling(MotionEvent arg0, MotionEvent arg1, float arg2, float arg3) {
            return false;
        }

        public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2, float arg3) {
            return true;
        }

        public void onShowPress(MotionEvent arg0) {
        }

        public boolean onSingleTapUp(MotionEvent arg0) {
            return false;
        }
    });

    public boolean onTouchEvent(MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    };
}

