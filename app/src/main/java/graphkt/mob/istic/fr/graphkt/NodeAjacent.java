package graphkt.mob.istic.fr.graphkt;

import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;

public class NodeAjacent implements Parcelable  {
    RectF first ;
    RectF second;
    private String etiquette ;
    private boolean isChange = false;
    RectF cadre_etiquette ;

    public NodeAjacent(RectF _first , RectF _second, String comment){
        first = _first;
        second = _second;
        etiquette = comment;
        isChange = false;
        cadre_etiquette = new RectF();
    }


    protected NodeAjacent(Parcel in) {
        first = in.readParcelable(RectF.class.getClassLoader());
        second = in.readParcelable(RectF.class.getClassLoader());
        etiquette = in.readString();
        isChange = in.readByte() != 0;
        cadre_etiquette = in.readParcelable(RectF.class.getClassLoader());
    }

    public static final Creator<NodeAjacent> CREATOR = new Creator<NodeAjacent>() {
        @Override
        public NodeAjacent createFromParcel(Parcel in) {
            return new NodeAjacent(in);
        }

        @Override
        public NodeAjacent[] newArray(int size) {
            return new NodeAjacent[size];
        }
    };

    public void setEtiquette(String comment){
        isChange =  true;
        etiquette = comment;
    }

    private boolean IsChange(){
        return this.isChange;
    }


    public String getEtiquette() {
        return etiquette;
    }

    public boolean isContains(RectF node){
        return (this.first == node || this.second == node);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(first, i);
        parcel.writeParcelable(second, i);
        parcel.writeParcelable(cadre_etiquette, i);
        parcel.writeString(etiquette);
        parcel.writeByte((byte) (isChange ? 1 : 0));
    }
}
