package graphkt.mob.istic.fr.graphkt;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.RectF;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;

public class CustomView extends View {

    private DrawableGraph drawableGraph ;


    public CustomView(Context context, DrawableGraph drawableGraphInput){
        super(context);
        this.drawableGraph = drawableGraphInput;
        this.drawableGraph.setColor(Color.RED);
    }

    public CustomView(Context context){
        super(context);
        Point size = new Point(1440, 2392);
        this.drawableGraph = new DrawableGraph(size);
        this.drawableGraph.setColor(Color.RED);
    }


    public void setDrawableGraph(DrawableGraph drawableGraph) {
        this.drawableGraph = drawableGraph;
    }

    protected void onDraw(Canvas canvas) {
        this.drawableGraph.draw(canvas);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        final int action  = event.getActionMasked();
        final int pointer = event.getActionIndex();

        switch (action) {

              /*
                MotionEvent.ACTION_DOWN :
                L'utilisateur vient d'appuyer sur l'écran.
                C'est la première valeur récupérée suite à une action sur l'écran
              */
            case MotionEvent.ACTION_DOWN :
                Point touchDown = new Point((int)event.getX(), (int)event.getY());
                this.drawableGraph.lookForIntersection(touchDown);
                break;

              /*
                MotionEvent.ACTION_MOVE : Fait suite à l'événement
                précédent et indique que l'utilisateur n'a pas relaché la pression sur l'écran et est en train de bouger
              */
            case MotionEvent.ACTION_MOVE :

                int count = 0;
                for(RectF rect : this.drawableGraph.getmActiveRects())
                {
                    Point curretPoint = new Point((int)event.getX(pointer), (int)event.getY());

                    int indexOf_node_toMove = this.drawableGraph.getGraph().getNodes().indexOf(rect);
                    if( indexOf_node_toMove != -1 ){
                        RectF node_to_move = this.drawableGraph.getGraph().getNodes().get(indexOf_node_toMove);
                        drawableGraph.moveNode(curretPoint, drawableGraph.getmActiveDragPoints().get(count), node_to_move);
                    }
                    count++;
                }
                invalidate();
                break;

              /*
               MotionEvent.ACTION_ACTION : Fait suite à l'événement
               Envoyé lorsque l'utilisateur cesse d'appuyer sur l'écran (on vide nos tableau de trackeurs);
              */
            case MotionEvent.ACTION_UP :
            case MotionEvent.ACTION_CANCEL :
                drawableGraph.getmActiveDragPoints().clear(); //  mActiveDragPoints.removeAll(mActiveDragPoints);
                drawableGraph.getmActiveRects().clear(); //   mActiveDragPoints.removeAll(mActiveDragPoints);
                break;



            case MotionEvent.ACTION_POINTER_DOWN :
                break;

            case MotionEvent.ACTION_POINTER_UP :
                break;


            default:
                break;
        }
        return true;
    }

}
