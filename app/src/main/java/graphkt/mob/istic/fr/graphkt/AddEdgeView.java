package graphkt.mob.istic.fr.graphkt;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.RectF;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static android.content.ContentValues.TAG;

public class AddEdgeView extends View {


    private DrawableGraph drawableGraph ;
    Dialog label;
    TextView closeDialog;
    EditText edit_text_arc_label;
    Button save;
    int last_arc;


    public AddEdgeView(Context context, DrawableGraph drawableGraphInput){
        super(context);
        this.drawableGraph = drawableGraphInput;
        this.drawableGraph.setColor(Color.RED);
        label = new Dialog(this.getContext());
        edit_text_arc_label = new EditText(this.getContext());
    }

    public AddEdgeView(Context context){
        super(context);
        Point size = new Point(1440, 2392);
        this.drawableGraph = new DrawableGraph(size);
        this.drawableGraph.setColor(Color.RED);
    }


    public void setDrawableGraph(DrawableGraph drawableGraph) {
        this.drawableGraph = drawableGraph;
    }

    protected void onDraw(Canvas canvas) {
        this.drawableGraph.draw(canvas);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        final int action  = event.getActionMasked();
        final int pointer = event.getActionIndex();

        switch (action) {

              /*
                MotionEvent.ACTION_DOWN :
                L'utilisateur vient d'appuyer sur l'écran.
                C'est la première valeur récupérée suite à une action sur l'écran
              */
            case MotionEvent.ACTION_DOWN :
                Point touchDown = new Point((int)event.getX(), (int)event.getY());
                this.drawableGraph.lookForIntersection(touchDown);
                break;

              /*
                MotionEvent.ACTION_MOVE : Fait suite à l'événement
                précédent et indique que l'utilisateur n'a pas relaché la pression sur l'écran et est en train de bouger
              */
            case MotionEvent.ACTION_MOVE :
                Point currenPoint = new Point((int)event.getX(pointer), (int)event.getY(pointer));
                drawableGraph.current_point =  currenPoint;
                invalidate();

                break;

              /*
               MotionEvent.ACTION_ACTION : Fait suite à l'événement
               Envoyé lorsque l'utilisateur cesse d'appuyer sur l'écran (on vide nos tableau de trackeurs);
              */
            case MotionEvent.ACTION_UP :
            case MotionEvent.ACTION_CANCEL :
                drawableGraph.current_point =  null;
                Point touchUp = new Point( (int) event.getX(pointer) , (int) event.getY(pointer));
                int node = drawableGraph.getIntersectionNodesIndex(touchUp);
                Log.e(TAG, "onTouchEvent: le noeud touche est "+node );
                int arcIsAdded = drawableGraph.addArc(touchUp);
                drawableGraph.getmActiveDragPoints().clear();
                drawableGraph.getmActiveRects().clear();
                invalidate();
                if(node != -1 && arcIsAdded==1)
                    ShowOncreateLabelDialog(touchUp);

                break;



            case MotionEvent.ACTION_POINTER_DOWN :
                break;

            case MotionEvent.ACTION_POINTER_UP :
                break;


            default:
                break;
        }
        return true;
    }

    public void ShowOncreateLabelDialog(final Point touchUp){

        label.setContentView(R.layout.arclabeldialog);
        closeDialog = (TextView) label.findViewById(R.id.label_txtclose);
        edit_text_arc_label = (EditText) label.findViewById(R.id.edit_text_arc_label);
        save = (Button) label.findViewById(R.id.btn_save_label);
        closeDialog.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                last_arc= drawableGraph.getGraph().getArcGraph().size()-1;
                drawableGraph.getGraph().getArcGraph().remove(last_arc);
                invalidate();
                label.dismiss();
            }
        });
        save.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){

                String new_label = edit_text_arc_label.getText().toString();
                last_arc = drawableGraph.getGraph().getArcGraph().size()-1;
                drawableGraph.getGraph().getArcGraph().get(last_arc).setEtiquette(new_label);
                invalidate();
                label.dismiss();

            }
        });

        label.show();

    }
}

