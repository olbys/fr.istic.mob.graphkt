package graphkt.mob.istic.fr.graphkt;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.RectF;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class ResetGraphView extends View {


    private DrawableGraph drawableGraph ;


    public ResetGraphView(Context context, DrawableGraph drawableGraphInput){
        super(context);
        this.drawableGraph = drawableGraphInput;
        this.drawableGraph.setColor(Color.RED);
    }

    public ResetGraphView(Context context){
        super(context);
        Point size = new Point(1440, 2392);
        this.drawableGraph = new DrawableGraph(size);
        this.drawableGraph.setColor(Color.RED);
    }


    public void setDrawableGraph(DrawableGraph drawableGraph) {
        this.drawableGraph = drawableGraph;
    }

    protected void onDraw(Canvas canvas) {
        this.drawableGraph.draw(canvas);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        final int action  = event.getActionMasked();
        final int pointer = event.getActionIndex();

        switch (action) {

              /*
                MotionEvent.ACTION_DOWN :
                L'utilisateur vient d'appuyer sur l'écran.
                C'est la première valeur récupérée suite à une action sur l'écran
              */
            case MotionEvent.ACTION_DOWN :
                Point touchDown = new Point((int)event.getX(), (int)event.getY());
                if(this.drawableGraph.getIntersectionNodesIndex(touchDown)== -1)
                    this.drawableGraph.addNode(touchDown);
                invalidate();

                break;
        }
        return true;
    }
}

