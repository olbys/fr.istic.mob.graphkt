package graphkt.mob.istic.fr.graphkt;

import android.graphics.Color;
import android.graphics.Point;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.util.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Graph implements Parcelable {

    int INITIAL_NODES = 9, INITIAL_DISTANT = 100;

    float x =  30.0f , y = 30.0f, sidelength = 100.0f;

    private ArrayList<RectF> nodes;

    private ArrayList<Integer> nodesColor;

    private ArrayList<String> nodesLabel;

    private Point deviceSize;

    // All methode for path
    private ArrayList <NodeAjacent> arcGraph;

    /**
     * Constructeur de la classe
     * @param point {@link Point}
     */
    public Graph(Point point) {

        deviceSize = point;
        nodes = new ArrayList<>();
        nodesColor = new ArrayList<>();
        nodesLabel = new ArrayList<>();
        arcGraph = new ArrayList<>();


        for (int i=0 ;  i < INITIAL_NODES ; ++i){

            nodes.add( new RectF( x ,y , x+sidelength, y+sidelength));
            nodesColor.add(Color.RED);
            nodesLabel.add(String.valueOf(i+1));
        }

        this.constructNodes();

    }


    protected Graph(Parcel in) {
        INITIAL_NODES = in.readInt();
        INITIAL_DISTANT = in.readInt();
        x = in.readFloat();
        y = in.readFloat();
        sidelength = in.readFloat();
        nodes = in.createTypedArrayList(RectF.CREATOR);
        deviceSize = in.readParcelable(Point.class.getClassLoader());
        arcGraph = in.createTypedArrayList(NodeAjacent.CREATOR);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(INITIAL_NODES);
        parcel.writeInt(INITIAL_DISTANT);
        parcel.writeFloat(x);
        parcel.writeFloat(y);
        parcel.writeFloat(sidelength);
        parcel.writeTypedList(nodes);
        parcel.writeParcelable(deviceSize, i);
        parcel.writeTypedList(arcGraph);
    }

    public static final Creator<Graph> CREATOR = new Creator<Graph>() {
        @Override
        public Graph createFromParcel(Parcel in) {
            return new Graph(in);
        }

        @Override
        public Graph[] newArray(int size) {
            return new Graph[size];
        }
    };

    /**
     * bottom ligne de coordonnees en bas rectF
     * right ligne de coordonnees a droite du rectF
     */
    public void constructNodes(){

        float x_increase = 0 , y_increase = 0;

        RectF node;

        for (int i=1 ; i < this.nodes.size() ; ++i){

            // on recupère le cercle dessiné avant
            node = this.getNodes().get(i-1);

            x_increase = node.right + INITIAL_DISTANT;
            x_increase += sidelength;

            y_increase = node.bottom + INITIAL_DISTANT;
            y_increase += sidelength;

            if(x_increase < deviceSize.x && y_increase < deviceSize.y ) {

                nodes.get(i).left = node.right + INITIAL_DISTANT;
                nodes.get(i).right = nodes.get(i).left + sidelength;

                nodes.get(i).top = node.top;
                nodes.get(i).bottom =  node.bottom;

            } else {

                nodes.get(i).top = node.bottom + INITIAL_DISTANT;
                nodes.get(i).bottom =  nodes.get(i).top + sidelength;

            }


        }
    }


    /**
     * Retourne la liste des noeuds du graphe
     * @return ArrayList <RectF>
     */
    public ArrayList<RectF> getNodes() {
        return nodes;
    }
    public ArrayList<Integer> getNodeColors() { return nodesColor; }
    public ArrayList<String> getNodeLabels() { return nodesLabel; }


    /**
     * ajoute de node dans le graph
     * @param node {@link RectF}
     */
    public void addNode(RectF node){
        nodes.add(node);
    }

    public void setNodeColor(int index, int color){ nodesColor.set(index,color); }
    public void setNodeLabel(int index, String label){ nodesLabel.set(index,label); }

    public void setNodes(ArrayList<RectF> nodes) {
        this.nodes = nodes;
    }



    // ++++++++++++++++  Methodes des ars  ++++++++++++++++++++


    /**
     *
     * les listes des noeuds pour qui il y
     * @return ArrayList <Pair <RectF, RectF>>
     */
    public ArrayList<NodeAjacent> getArcGraph() {
        return arcGraph;
    }



    /**
     *
     * @param node {@link RectF}
     * @param nodeAdjacent {@link RectF}
     */
    public void linkTwoNode(final RectF node ,  final RectF nodeAdjacent){

        String text_of_arc = this.getNodeLabels().get(this.getNodes().indexOf(node));
        text_of_arc += " -> "+this.getNodeLabels().get(this.getNodes().indexOf(nodeAdjacent));
        NodeAjacent arc_entre =  new NodeAjacent(node , nodeAdjacent , text_of_arc);
        this.getArcGraph().add(arc_entre);
    }


    /**
     * suppression relationarc between two nodes
     * @param supress_node {@link RectF}
     */
    public void suppresLink(final RectF supress_node){

      Iterator<NodeAjacent>  it = this.getArcGraph().iterator();
      while(it.hasNext()){
              if (it.next().isContains(supress_node)){
                  it.remove();
              }
          }
    }


}
