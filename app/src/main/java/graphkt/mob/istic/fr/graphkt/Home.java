package graphkt.mob.istic.fr.graphkt;

import android.app.Application;
import android.content.Context;

/**
 * Created by mac on 06/11/2019.
 */

public class Home extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base,"fr"));
    }
}
