package graphkt.mob.istic.fr.graphkt;

import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    DrawableGraph mainDrawableGraph ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        mainDrawableGraph =  new DrawableGraph(size);

        if(savedInstanceState !=null ){
            Graph graph =  savedInstanceState.getParcelable("graph");
            this.mainDrawableGraph.setGraph(graph);
        }

        View customView =  new CustomView(this ,mainDrawableGraph);

        setContentView(customView);

    }

    @Override
    public void onSaveInstanceState(Bundle bundle){
        super.onSaveInstanceState(bundle);
        bundle.putParcelable("graph", this.mainDrawableGraph.getGraph());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //ajoute les entrées de menu_test à l'ActionBar
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    //gère le click sur une action de l'ActionBar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.item_normal:
                View customview= new CustomView(this ,mainDrawableGraph);
                this.setContentView(customview);
                Toast.makeText(this, R.string.item_normal, Toast.LENGTH_LONG).show();
                return true;
            case R.id.item_add_node:
                View addNode= new AddNodeView(this ,mainDrawableGraph);
                this.setContentView(addNode);
                Toast.makeText(this,"Mode "+getString(R.string.item_add_node), Toast.LENGTH_LONG).show();
                return true;
            case R.id.item_add_arc:
                View addEdge= new AddEdgeView(this ,mainDrawableGraph);
                this.setContentView(addEdge);
                Toast.makeText(this,"Mode "+ getString(R.string.item_add_arc), Toast.LENGTH_LONG).show();
                return true;

            case R.id.item_release:
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                mainDrawableGraph.reinitialize(size);
                View relatedView= new CustomView(this ,mainDrawableGraph);
                this.setContentView(relatedView);
                return true;

            case R.id.item_edit_noeud_arc:
                View editNodeView= new EditNodeView(this,mainDrawableGraph);
                this.setContentView(editNodeView);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
    }



}
