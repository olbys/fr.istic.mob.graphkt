package graphkt.mob.istic.fr.graphkt;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;


public class DrawableGraph extends Drawable {



    final float RADIUS = 159;

    final int INVALID_INDEX = -1 ;

    private Graph graph;

    public Paint myPaint;

    private ArrayList<Point> mActiveDragPoints;
    private ArrayList<RectF>  mActiveRects;

    public Point current_point ;



    public DrawableGraph(Point point){
        super();
        this.graph = new Graph(point);
        init();

    }

    /**
     * initialze les attributs du graphe
     */
    private void init(){
        myPaint =  new Paint();
        myPaint.setColor(Color.BLACK);
        myPaint.setStrokeWidth(10);
        mActiveDragPoints = new ArrayList<>(9);
        mActiveRects = new ArrayList<>(9);

    }


    public void setGraph(Graph graph){
        this.graph=graph;
    }

    /**
     * reiniatilise le graphe a la case départ avec les 9 noeuds de départ et sans arc
     */
    public void reinitialize(Point point){

       graph = new Graph(point) ;
       invalidateSelf();

    }



    @Override
    public void draw(@NonNull Canvas canvas) {



        // paint pour les arc des nodees
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(20);
        // Dessiner les arcs entre les noeuds
        if( current_point !=null && !this.getmActiveDragPoints().isEmpty()){
            int node_initial = this.getIntersectionNodesIndex(this.mActiveDragPoints.get(0));
            if(node_initial != INVALID_INDEX){
                paint.setColor(this.graph.getNodeColors().get(node_initial));
                Path temporaire = new Path();
                temporaire.moveTo(this.mActiveDragPoints.get(0).x , this.mActiveDragPoints.get(0).y);
                temporaire.lineTo(current_point.x , current_point.y);
                canvas.drawPath(temporaire , paint);
            }
        }


        this.drawArc(canvas , paint , this.graph);


        // paint pour les text a ecrire dans les etiquettes
        Paint paintText= new Paint();
        paintText.setColor(Color.WHITE);
        paintText.setTextSize(50);  //set text size
        float textHeight = paintText.descent() - paintText.ascent();
        float textOffset = (textHeight / 2) - paintText.descent();
        paintText.setTextAlign(Paint.Align.CENTER);


        for(int i=0; i<graph.getNodes().size();++i){

            myPaint.setColor(getGraph().getNodeColors().get(i));
            myPaint.setStrokeWidth(30);
            myPaint.setStyle(Paint.Style.FILL_AND_STROKE);
            RectF rect=graph.getNodes().get(i);
            String label= graph.getNodeLabels().get(i);

            canvas.drawRoundRect(rect, RADIUS, RADIUS, myPaint);
            canvas.drawText(label , rect.centerX() , rect.centerY()+textOffset, paintText );


        }




    }



    /**
     * @param point
     * retoune l'index du noeud dans la liste des nodes qui correspond au point toucher sur l'ecran
     * @return int
     */
    public int getIntersectionNodesIndex(Point point){

        int index = INVALID_INDEX;

        // parcourir l'ensemble des noeuds du graphe et verifier s'il y a une intersection avec point
        for(RectF node : this.graph.getNodes()){

            if( node.contains(point.x, point.y)){
                index =  this.graph.getNodes().indexOf(node);
                return index;
            }
        }

        return index;

    }


    /**
     *
     * @param touchdownEventPoint {@link Point}
     * Ajoute le noeud correspondant au point, dans la liste des noeuds courrants cad qui sont selectionnes
     */
    public void lookForIntersection(Point touchdownEventPoint){

        final int index = getIntersectionNodesIndex(touchdownEventPoint);

        // si c'est un noeud qui est selectionnee
        if( index != INVALID_INDEX)
        {
            final RectF node =  this.graph.getNodes().get(index);
            // si le noeud n'est pas dans les noeuds Actives on l'ajoute en et on enregistre egalement
            // la position initiale du touchDown
            if( mActiveRects.indexOf(node) == INVALID_INDEX )
            {
                mActiveDragPoints.add(touchdownEventPoint);
                mActiveRects.add(node);

            }


        }

    }




    /**
     *
     * @param currentPoint {@link Point}
     * @param prevPoint {@link Point}
     * @param rect {@link RectF}
     * met a jour les cordoonnes du point courrant (liste mRectAtive) avec point de l'index courrant
     */
    public void  moveNode(Point currentPoint, Point prevPoint, final RectF rect) {
        int xMoved = currentPoint.x - prevPoint.x;
        int yMoved = currentPoint.y - prevPoint.y;
        rect.set(rect.left + xMoved, rect.top + yMoved, rect.right + xMoved, rect.bottom + yMoved);
        mActiveDragPoints.set(mActiveDragPoints.indexOf(prevPoint), currentPoint);
    }



    /**
     * ajoute un nouveau node dans le graphe
     * @param point {@link Point}
     */
    public void addNode(Point point){

        float x = (float) point.x ;
        float y = (float) point.y ;
        RectF rect = new RectF(x , y, x+graph.sidelength, y+graph.sidelength);
        graph.addNode(rect);
    }


    @Override
    public void setAlpha(int alpha) {
        myPaint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        myPaint.setColorFilter(cf);
    }

    public void setColor(int color){
        myPaint.setColor(color);

    }


    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }

    /**
     * retourne le graphe a construire
     * @return Graph
     */
    public Graph getGraph() {
        return graph;
    }

    /**
     *
     * retourne la liste des points active de l'index
     * @return ArrayList <Point>
     */
    public ArrayList<Point> getmActiveDragPoints() {
        return mActiveDragPoints;
    }


    /**
     * retourne la liste de noeuds active
     * @return ArrayList <RectF>
     */
    public ArrayList<RectF> getmActiveRects() {
        return mActiveRects;
    }


    // #################### GERER LES POINTS ET ARC #####################3

    /**
     *
     * @param first_point {@link Point}
     * @param second_point {@link Point}
     * @return
     */
    public boolean pointAreSameNode(Point first_point , Point second_point) {
         int indexNode_first_point = this.getIntersectionNodesIndex(first_point);
         int indexNode_second_point = this.getIntersectionNodesIndex(second_point);
         return ( (indexNode_first_point == indexNode_second_point) && indexNode_first_point != INVALID_INDEX );
    }


    /**
     * [MODE] = AddEdgeView
     * Appeller lorsque le doigt bouge sur l'ecran
     * @param currentIndex {@link Point}
     */
    public int addArc(Point currentIndex){

        if(this.mActiveRects.isEmpty()) return -1;

        Point previous_point = this.mActiveDragPoints.get(0);
        int indexNode = this.getIntersectionNodesIndex(currentIndex);

        // si le point est sur meme noeud alors ne rien faire
        if ( pointAreSameNode(previous_point , currentIndex) || indexNode == INVALID_INDEX) return -1;

        this.getGraph().linkTwoNode(mActiveRects.get(0) , this.getGraph().getNodes().get(indexNode));

        return 1;
    }


    /**
     * Dessiner tous les arcs existant entre les noeuds
     * @param canvas {@link Canvas}
     * @param paint {@link Paint}
     * @param graph {@link Graph}
     */
    private void drawArc(Canvas canvas ,Paint paint ,  final Graph graph){

        for (NodeAjacent lien_between_nodes :  graph.getArcGraph()){
            Path arc = new Path();
            int first_position = this.getGraph().getNodes().indexOf(lien_between_nodes.first);
            if(first_position != INVALID_INDEX){
                paint.setColor(this.graph.getNodeColors().get(first_position));
            }

            arc.moveTo(lien_between_nodes.first.centerX() , lien_between_nodes.first.centerY());
            arc.lineTo(lien_between_nodes.second.centerX() , lien_between_nodes.second.centerY());
            canvas.drawPath(arc ,  paint);


            //Paint pour dessiner l'arc
            Paint etiq_arcPaint = new Paint();
            etiq_arcPaint.setStyle(Paint.Style.STROKE);
            etiq_arcPaint.setColor(Color.WHITE);
            etiq_arcPaint.setTextSize(45);
            etiq_arcPaint.setTextAlign(Paint.Align.CENTER);
            String text_of_arc = lien_between_nodes.getEtiquette();

            // Path mesure pour le milieu de l'arc

            PathMeasure pm = new PathMeasure(arc , false);
            float midleCoordonnate [] = {0f, 0f};
            //get coordinates of the middle point
            pm.getPosTan(pm.getLength()*0.5f, midleCoordonnate, null);



            float etiquet_width = etiq_arcPaint.measureText(text_of_arc);

            // Ajouter +10 volontairement pour donner plus d'espace dans le cadre de l'etiaquette
            int cadre_with = (int) (etiquet_width)+20;


            Rect bounds =  new Rect();
            etiq_arcPaint.getTextBounds(text_of_arc , 0 , text_of_arc.length() , bounds);
            // Ajouter +15 volontairement pour donner plus d'espace dans le cadre de l'etiaquette
            int cadre_heigth = bounds.height()+30;

            int xPos = (int) (midleCoordonnate[0] - cadre_with/2);
            int yPos = (int) (midleCoordonnate[1]-cadre_heigth/2);




            RectF cadre_arc_etiquette = new RectF(xPos ,yPos ,xPos+cadre_with , yPos+cadre_heigth );
            lien_between_nodes.cadre_etiquette = cadre_arc_etiquette;


            canvas.drawRect(cadre_arc_etiquette , paint);

            float textHeight = etiq_arcPaint.descent() - etiq_arcPaint.ascent();
            float textOffset = (textHeight / 2) - etiq_arcPaint.descent();

            canvas.drawText(text_of_arc , cadre_arc_etiquette.centerX() , cadre_arc_etiquette.centerY()+textOffset , etiq_arcPaint);

        }



    }


    /**
     * suppression relationarc between two nodes
     * @param supress_node {@link RectF}
     */
    public void suppresLink(RectF supress_node){
        this.graph.suppresLink(supress_node);
    }


    /**
     *
     * @param index {@link Point}
     * @return boolean
     * retourn l'index
     */
    public int etiquetteOfArc(Point index){

       for (NodeAjacent nodeAjacent : this.getGraph().getArcGraph()){
            if(nodeAjacent.cadre_etiquette.contains(index.x, index.y)){
                return this.getGraph().getArcGraph().indexOf(nodeAjacent);
            }
       }
        return INVALID_INDEX;
    }

    /**
     *
     * @param node
     * @param text
     */
    public void nodeEtiquetteAdapter(int node ,  String text){

        Paint paintText= new Paint();
        paintText.setTextSize(50);  //set text size
        float content_width = paintText.measureText(text)+70;
        RectF rect = this.getGraph().getNodes().get(node);
        rect.set(rect.left , rect.top, rect.left+content_width , rect.bottom);
    }

    public void nodeLengthAdapter(int node , float newlength){

        RectF rect = this.getGraph().getNodes().get(node);
        String label = this.getGraph().getNodeLabels().get(node);
        Paint paintText= new Paint();
        paintText.setTextSize(50);
        float content_width = paintText.measureText(label)+70;

        if(newlength>=content_width)
            rect.set(rect.left , rect.top, rect.left+newlength , rect.top+newlength);

    }








}
